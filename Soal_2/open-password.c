#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#define BUFFER_SIZE 1024

int base64_decode(const char *input, unsigned char **output);

int main() {
    FILE *file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        return 1;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);
    char *buffer = (char *)malloc(file_size + 1);
    if (buffer == NULL) {
        fclose(file);
        return 1;
    }

    fread(buffer, 1, file_size, file);
    buffer[file_size] = '\0';

    int decoded_size = (file_size * 3) / 4;
    unsigned char *decoded_buffer = (unsigned char *)malloc(decoded_size + 1);
    if (decoded_buffer == NULL) {
        fclose(file);
        free(buffer);
        return 1;
    }

    int result = base64_decode(buffer, &decoded_buffer);
    if (result < 0) {
        fclose(file);
        free(buffer);
        free(decoded_buffer);
        return 1;
    }

    decoded_buffer[result] = '\0'; 
    printf("Decoded Result:\n%s\n", (char *)decoded_buffer);

    fclose(file);
    free(buffer);

    const char *zip_file = "home.zip";
    const char *password = (char *)decoded_buffer;
    pid_t child_pid = fork();

    if (child_pid == -1) {
        return 1;
    }

    if (child_pid == 0) {
        execlp("unzip", "unzip", "-P", password, zip_file, "-d", ".", NULL);
        exit(1);
    } else {
        int status;
        wait(&status);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            if (exit_status != 0) {
                // Handle failure if needed
            }
        }
    }

    free(decoded_buffer);
    return 0;
}

int base64_decode(const char *input, unsigned char **output) {
    const char *chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int i, j;
    unsigned char a3[3], a4[4];
    int input_length = strlen(input);
    int output_length = 0;

    for (i = 0; i < input_length; i += 4) {
        for (j = 0; j < 4; j++) {
            if (input[i + j] == '=') {
                a4[j] = 0;
            } else {
                const char *found = strchr(chars, input[i + j]);
                if (found == NULL) {
                    return -1;
                }
                a4[j] = found - chars;
            }
        }

        a3[0] = (a4[0] << 2) | (a4[1] >> 4);
        a3[1] = (a4[1] << 4) | (a4[2] >> 2);
        a3[2] = (a4[2] << 6) | a4[3];

        for (j = 0; j < 3; j++) {
            if (output_length >= BUFFER_SIZE) {
                return -1;
            }
            (*output)[output_length++] = a3[j];
        }
    }

    return output_length;
}