
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

static const char *log_file = "logs-fuse.log";
static const char *source_dir = "/home/khansaadiar/soal2";
static const char *target_dir = "/home/khansaadiar/soal2";


void write_log(const char *status, const char *tag, const char *info) {
   time_t t;
   struct tm *tm_info;
   char timestamp[20];


   time(&t);
   tm_info = localtime(&t);
   strftime(timestamp, 20, "%d/%m/%Y-%H:%M:%S", tm_info);


   FILE *log = fopen(log_file, "a");
   if (log) {
       fprintf(log, "[%s]::%s::[%s]::%s\n", status, timestamp, tag, info);
       fclose(log);
   }
}


static int move_files(const char *source, const char *target, const char *ext, const char *category) {
   DIR *dir = opendir(source);
   if (dir == NULL) {
       perror("gagal membuka source directory");
       return -1;
   }


   struct dirent *entry;
   char source_path[PATH_MAX];
   char target_path[PATH_MAX];


   while ((entry = readdir(dir)) != NULL) {
       if (entry->d_type == DT_REG) { 
           char *file_ext = strrchr(entry->d_name, '.');
           if (file_ext != NULL && strcmp(file_ext, ext) == 0) {
               snprintf(target_path, PATH_MAX, "%s/%s", target, category);
               mkdir(target_path, 0755);

               snprintf(source_path, PATH_MAX, "%s/%s", source, entry->d_name);
               snprintf(target_path, PATH_MAX, "%s/%s/%s", target, category, entry->d_name);
               rename(source_path, target_path);

               write_log("SUCCESS", "move_files", entry->d_name);
           }
       }
   }

   closedir(dir);
   return 0;
}


static int custom_getattr(const char *path, struct stat *stbuf) {
   int res = 0;
   memset(stbuf, 0, sizeof(struct stat));


   if (strcmp(path, "/") == 0) {
       stbuf->st_mode = S_IFDIR | 0755;
       stbuf->st_nlink = 2;
   } else if (strcmp(path, "/documents") == 0 || strcmp(path, "/images") == 0 ||
              strcmp(path, "/website") == 0 || strcmp(path, "/sisop") == 0 ||
              strcmp(path, "/text") == 0 || strcmp(path, "/aI") == 0) {
       stbuf->st_mode = S_IFDIR | 0755;
       stbuf->st_nlink = 2;
   } else {
       res = -ENOENT;
   }


   return res;
}


static int custom_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi) {
   (void) offset;
   (void) fi;


   if (strcmp(path, "/") == 0) {
       filler(buf, ".", NULL, 0);
       filler(buf, "..", NULL, 0);
       filler(buf, "documents", NULL, 0);
       filler(buf, "images", NULL, 0);
       filler(buf, "website", NULL, 0);
       filler(buf, "sisop", NULL, 0);
       filler(buf, "text", NULL, 0);
       filler(buf, "aI", NULL, 0);

       write_log("SUCCESS", "readdir", path);


       return 0;
   }


   return -ENOENT;
}


static int custom_open(const char *path, struct fuse_file_info *fi) {
   (void) path;
   (void) fi;
   return 0;
}


static struct fuse_operations custom_oper = {
   .getattr = custom_getattr,
   .readdir = custom_readdir,
   .open = custom_open,
};


int main(int argc, char *argv[]) {
   if (argc != 2) {
       fprintf(stderr, "Usage: %s <mountpoint>\n", argv[0]);
       return EXIT_FAILURE;
   }


   if (move_files(source_dir, target_dir, ".pdf", "documents") != 0 ||
       move_files(source_dir, target_dir, ".docx", "documents") != 0 ||
       move_files(source_dir, target_dir, ".jpg", "images") != 0 ||
       move_files(source_dir, target_dir, ".png", "images") != 0 ||
       move_files(source_dir, target_dir, ".ico", "images") != 0 ||
       move_files(source_dir, target_dir, ".js", "website") != 0 ||
       move_files(source_dir, target_dir, ".html", "website") != 0 ||
       move_files(source_dir, target_dir, ".json", "website") != 0 ||
       move_files(source_dir, target_dir, ".c", "sisop") != 0 ||
       move_files(source_dir, target_dir, ".sh", "sisop") != 0 ||
       move_files(source_dir, target_dir, ".txt", "text") != 0 ||
       move_files(source_dir, target_dir, ".ipynb", "aI") != 0 ||
       move_files(source_dir, target_dir, ".csv", "aI") != 0) {
       fprintf(stderr, "Error moving files\n");
       return EXIT_FAILURE;
   }


   return fuse_main(argc, argv, &custom_oper, NULL);
}