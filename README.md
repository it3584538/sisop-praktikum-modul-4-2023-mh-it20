# sisop-praktikum-modul-4-2023-MH-IT20
Laporan pengerjaan soal shift modul 4 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal 1

### Study case soal 1
---
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.

### Problem
---
a Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:

- Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
```c
Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
Output: eTgj80q7Ut7eNhNVooBE.HEIC
```

- Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus.
```c
Ex: "mv coba-deh.jpg delete-foto/" 
```

b. Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini.

- Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."

-  Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  

c. Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
- Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.

- Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.

- Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.

- Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.

d. Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus.

-  Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas).

e. Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
```c
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:
[SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6
```

### Solution
---
[Source Code](./Soal_1)

Permasalahan diatas dapat diatas dengan Code tersebut yang merupakan implementasi dari FUSE (Filesystem in Userspace) dalam bahasa C. FUSE memungkinkan pengembang untuk membuat sistem berkas di ruang pengguna (userspace) sebagai program biasa. Code tersebut mengimplementasikan fungsi-fungsi operasi dasar pada sistem berkas, seperti getattr, readdir, rename, mkdir, open, read, dan unlink.

1. Pembalikan Nama File dalam Folder "rev":

Folder dengan prefix "rev" akan melakukan pembalikan nama file saat dipindahkan ke dalamnya. Ini sesuai dengan fungsi xmp_rename pada code sebelumnya yang menangani pembalikan nama file ketika ada prefix "rev" pada nama yang baru.
```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    //Decode file
    if (strncmp(path, "tulisan/", 8) == 0)
    {
        char content[1000];
        FILE *file = fopen(fpath, "r");
        if (file == NULL)
            return -errno;

        if (fscanf(file, "%s", content) == 1)
        {
            fclose(file);

            //check for prefixes
            if (strncmp(content, "base64", 7) == 0)
            {
                char decoded[1000];
                decode_base64(content + 7, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "rot13", 6) == 0)
            {
                char decoded[1000];
                decode_rot13(content + 6, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "hex", 4) == 0)
            {
                char decoded[1000];
                decode_hex(content + 4, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "rev", 4) == 0)
            {
                char decoded[1000];
                reverse_text(content + 4, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
        }
        else
        {
            fclose(file);
        }
    }

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res;
    DIR *dp;
    struct dirent *de;
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);
    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;
    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];
    sprintf(ffrom, "%s%s", base_path, from);
    sprintf(fto, "%s%s", base_path, to);
    res = rename(ffrom, fto);
    if (res == -1)
        return -errno;
    if (strncmp(to, "rev", 4) == 0) {
    
    char reversed_name[1000];
    char extension[1000];
    char *dot = strrchr(fto, '.'); 
    int len = dot - fto; 
    int i, j;
    for (i = len - 1, j = 0; i >= 0; i--, j++) {
        reversed_name[j] = fto[i];
    }
    reversed_name[j] = '\0';

    strcpy(extension, dot);
    char frename[2000];
    sprintf(frename, "%s%s/%s%s", base_path, from, reversed_name, extension);
    res = rename(ffrom, frename);
    if (res == -1)
        return -errno;
}
    if (strncmp(to, "delete", 7) == 0) {
        res = unlink(fto);
        if (res == -1)
            return -errno;
    }
    return 0;
}
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);

    int res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    return 0;
}
```
2. Penghapusan Gambar dalam Folder "delete":

Folder dengan prefix "delete" akan langsung menghapus file yang dipindahkan ke dalamnya. Hal ini disesuaikan dengan implementasi dari fungsi xmp_rename pada kode sebelumnya yang menangani penghapusan file jika terdapat prefix "delete" pada nama yang baru.
```c
    if (strncmp(to, "delete", 7) == 0) {
        res = unlink(fto);
        if (res == -1)
            return -errno;
    }
    return 0;
}
```
sama seperti code diatas.

3. Perubahan Permission pada File "script.sh":

Untuk mencegah skrip script.sh agar tidak dapat menghapus isi folder "gallery", "sisop", dan "tulisan", Anthoni dan timnya harus mengatur permission yang tepat pada file tersebut. Kemungkinan mereka akan menggunakan fungsi sistem operasi seperti chmod untuk mengubah permission agar file tidak dapat dijalankan atau diubah tanpa izin.
```c
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];
    sprintf(ffrom, "%s%s", base_path, from);
    sprintf(fto, "%s%s", base_path, to);
    res = rename(ffrom, fto);
    if (res == -1)
        return -errno;
    if (strncmp(to, "rev", 4) == 0) {
    
    char reversed_name[1000];
    char extension[1000];
    char *dot = strrchr(fto, '.'); 
    int len = dot - fto; 
    int i, j;
    for (i = len - 1, j = 0; i >= 0; i--, j++) {
        reversed_name[j] = fto[i];
    }
    reversed_name[j] = '\0';

    strcpy(extension, dot);
    char frename[2000];
    sprintf(frename, "%s%s/%s%s", base_path, from, reversed_name, extension);
    res = rename(ffrom, frename);
    if (res == -1)
        return -errno;
}
    if (strncmp(to, "delete", 7) == 0) {
        res = unlink(fto);
        if (res == -1)
            return -errno;
    }
    return 0;
}
```

5. Fitur Baru "test" dengan Isi File yang Terbalik:

Untuk membuat file dengan prefix "test" yang isi file-nya terbalik, Anthoni dan timnya bisa menggunakan algoritma pembalikan teks atau isi file yang telah diimplementasikan pada fungsi reverse_text pada kode sebelumnya.
```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);

    int res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}
```


6. Manipulasi File Teks pada Folder "tulisan" menggunakan Fuse:

Untuk memanipulasi file-file pada folder "tulisan" sesuai dengan prefix yang ada, seperti Base64, ROT13, atau representasi heksadesimal, mereka dapat memanfaatkan FUSE. Implementasi dari kode sebelumnya telah menyediakan operasi-operasi tersebut dalam fungsi-fungsi seperti xmp_getattr untuk melakukan dekripsi pada file-file yang sesuai dengan prefix yang ada.
```c
//decode base64
static void decode_base64(const char *input, char *output)
{
    BIO *b64, *bio, *mem;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    mem = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, mem);

    BIO_write(bio, input, strlen(input));
    BIO_flush(bio);

    BUF_MEM *bptr;
    BIO_get_mem_ptr(bio, &bptr);


    strncpy(output, bptr->data, bptr->length);
    output[bptr->length] = '\0';

    BIO_free_all(bio);
}

//decode ROT13
static void decode_rot13(const char *input, char *output)
{
    for (int i = 0; i < strlen(input); ++i)
    {
        if (isalpha(input[i]))
        {
            char base = islower(input[i]) ? 'a' : 'A';
            output[i] = base + (input[i] - base + 13) % 26;
        }
        else
        {
            output[i] = input[i];
        }
    }
    output[strlen(input)] = '\0';
}

//decode hexadecimal
static void decode_hex(const char *input, char *output)
{
    int len = strlen(input);
    for (int i = 0, j = 0; i < len; i += 2, ++j)
    {
        sscanf(input + i, "%2x", (unsigned int *)&output[j]);
    }
    output[len / 2] = '\0';
}

//reverse text content
static void reverse_text(const char *input, char *output)
{
    int len = strlen(input);
    for (int i = len - 1, j = 0; i >= 0; --i, ++j)
    {
        output[j] = input[i];
    }
    output[len] = '\0';
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", base_path, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    //Decode file
    if (strncmp(path, "tulisan/", 8) == 0)
    {
        char content[1000];
        FILE *file = fopen(fpath, "r");
        if (file == NULL)
            return -errno;

        if (fscanf(file, "%s", content) == 1)
        {
            fclose(file);

            //check for prefixes
            if (strncmp(content, "base64", 7) == 0)
            {
                char decoded[1000];
                decode_base64(content + 7, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "rot13", 6) == 0)
            {
                char decoded[1000];
                decode_rot13(content + 6, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "hex", 4) == 0)
            {
                char decoded[1000];
                decode_hex(content + 4, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
            else if (strncmp(content, "rev", 4) == 0)
            {
                char decoded[1000];
                reverse_text(content + 4, decoded);
                FILE *decoded_file = fopen(fpath, "w");
                fprintf(decoded_file, "%s", decoded);
                fclose(decoded_file);
            }
        }
        else
        {
            fclose(file);
        }
    }

    return 0;
}
```

7. Kebijakan Keamanan pada Folder dengan Prefix "disable":

Untuk memberlakukan kebijakan khusus pada folder dengan prefix "disable" agar tidak dapat diakses tanpa izin, Anthoni dan timnya bisa menggunakan mekanisme seperti password protection atau enkripsi. Mereka perlu mengimplementasikan kontrol akses pada folder tersebut sehingga hanya pengguna yang memiliki izin atau password tertentu yang dapat mengaksesnya. Logs dari setiap proses yang dilakukan dapat dicatat menggunakan fungsi log yang memantau operasi-operasi pada FUSE, seperti yang didefinisikan dalam contoh log format yang diberikan.
```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
    .open = xmp_open,
    .read = xmp_read,
    .unlink = xmp_unlink,
};
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Kemudian yang terakhir dengan cara compile file code tersebut yang bernmama "hell.c" dengan cara:
```c
gcc -Wall 'pkg-config fuse --cflags' hell.c -o fuse 'pkg-config fuse --libs'
```

Kemudian di run di folder yang akan kita taruh dengan cara:
```c
./fuse target
```

maka hasilnya akan masuk semua pada folder target.

### Kendala
---
1. Tidak bisa meng-compile file "hell.c" terdapat eror pada pengcompile an tersebut.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Memperbaiki compile yang eror dengan sedikit menambahkan code dan meng-install library yang belum tersedia

### Hasil
---

1. Hasil Run hell.c `hell.c`

![hasil File 'hell.c'](img/Soal_1/1.png)

2. Hasil folder dari run hell.c `hell.c`

![hasil File 'hell.c'](img/Soal_1/2.png)


## Soal 2

### Study case soal 2
---
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

### Problem
---
a Membuat file open-password.c untuk membaca file zip-pass.txt

- Melakukan proses dekripsi base64 terhadap file tersebut

- Lalu unzip home.zip menggunakan password hasil dekripsi

b. Membuat file semangat.c

- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
```c
**[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:**
[SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop
```

- Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
    - Melakukan unzip pada home.zip
    - Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
        - documents: .pdf dan .docx. 
        - images: .jpg, .png, dan .ico. 
        - website: .js, .html, dan .json. 
        - sisop: .c dan .sh. 
        - text: .txt. 
        - aI: .ipynb dan .csv.

c. Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu

- Password terdapat di file password.bin

d. Pada folder website

- Membuat file csv pada folder website dengan format ini: file,title,body

- Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

- Tidak dapat menghapus file / folder yang mengandung prefix “restricted”

- Pada folder documents
    - Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut

e. Membuat file server.c

- Pada folder ai, terdapat file webtoon.csv

- Manda diminta untuk membuat sebuah server (socket programming) untuk 
membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client.
    - Menampilkan seluruh judul
    - Menampilkan berdasarkan genre
    - Menampilkan berdasarkan hari
    - Menambahkan ke dalam file webtoon.csv
    - Melakukan delete berdasarkan judul
    - Selain command yang diberikan akan menampilkan tulisan “Invalid Command”

- Manfaatkan client.c pada folder sisop sebagai client 

### Solution
---
[Source Code](./Soal_2)

a.	open-password.c
Program ini dapat membaca file zip-pass.txt yang didapatkan setelah unzip manual dari files.zip yang didownload. File zip-pass.txt ini berisi password ter-encode, selanjutnya program ini dapat melakukan decode base-64, dan menggunakan hasil decode sebagai password untuk mengekstrak file home.zip.

-	Membuka file zip-pass.txt dan alokasi memory
```c
FILE *file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        return 1;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);
    char *buffer = (char *)malloc(file_size + 1);
    if (buffer == NULL) {
        fclose(file);
        return 1;
    }
```

Program membuka file "zip-pass.txt" yang berisi password yang diencode dengan base64. Selanjutnya, program mengalokasikan memori untuk menyimpan isi file dan password.

-	Decode Base64
```c
int decoded_size = (file_size * 3) / 4;
    unsigned char *decoded_buffer = (unsigned char *)malloc(decoded_size + 1);
    if (decoded_buffer == NULL) {
        fclose(file);
        free(buffer);
        return 1;
    }

    int result = base64_decode(buffer, &decoded_buffer);
    if (result < 0) {
        fclose(file);
        free(buffer);
        free(decoded_buffer);
        return 1;
    }
```

Program menghitung ukuran buffer yang dibutuhkan untuk menyimpan hasil decode base64. Memori dialokasikan dan fungsi base64_decode dipanggil untuk melakukan decode.

-	Ekstraksi File Zip
```c
const char *zip_file = "home.zip";
    const char *password = (char *)decoded_buffer;
    pid_t child_pid = fork();

    if (child_pid == -1) {
        return 1;
    }

    if (child_pid == 0) {
        execlp("unzip", "unzip", "-P", password, zip_file, "-d", ".", NULL);
        exit(1);
    } else {
        int status;
        wait(&status);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            if (exit_status != 0) {
                // Handle failure if needed
            }
        }
    }
```

Program mengekstrak file zip menggunakan perintah unzip dengan password yang telah di-decode. Proses fork() digunakan untuk membuat proses anak yang akan menjalankan perintah unzip. Proses induk menunggu anak selesai dan mengecek status keluaran.

-	Fungsi Base64 Decode
```c
int base64_decode(const char *input, unsigned char **output) {
    const char *chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int i, j;
    unsigned char a3[3], a4[4];
    int input_length = strlen(input);
    int output_length = 0;

    for (i = 0; i < input_length; i += 4) {
        for (j = 0; j < 4; j++) {
            if (input[i + j] == '=') {
                a4[j] = 0;
            } else {
                const char *found = strchr(chars, input[i + j]);
                if (found == NULL) {
                    return -1;
                }
                a4[j] = found - chars;
            }
        }

        a3[0] = (a4[0] << 2) | (a4[1] >> 4);
        a3[1] = (a4[1] << 4) | (a4[2] >> 2);
        a3[2] = (a4[2] << 6) | a4[3];

        for (j = 0; j < 3; j++) {
            if (output_length >= BUFFER_SIZE) {
                return -1;
            }
            (*output)[output_length++] = a3[j];
        }
    }

    return output_length;
}
```
Fungsi base64_decode menerima string input yang diencode base64 dan menghasilkan output dalam bentuk unsigned char. Fungsi ini menggunakan tabel karakter base64 untuk melakukan decode.

b.	semangat.c
Program ini menggunakan FUSE yang dapat melakukan pemindahan file berdasarkan ekstensi ke direktori tertentu. Program juga mencatat aktivitas dalam file log. 
-	Inisialisasi dan Deklarasi Konstanta
```c
static const char *log_file = "logs-fuse.log";
static const char *source_dir = "/home/khansaadiar/soal2";
static const char *target_dir = "/home/khansaadiar/soal2";
```

Mendeklarasikan beberapa konstanta yaitu nama file log, direktori sumber, dan direktori target.

-	Fungsi Write Log
```c
void write_log(const char *status, const char *tag, const char *info) {
   time_t t;
   struct tm *tm_info;
   char timestamp[20];

   time(&t);
   tm_info = localtime(&t);
   strftime(timestamp, 20, "%d/%m/%Y-%H:%M:%S", tm_info);

   FILE *log = fopen(log_file, "a");
   if (log) {
       fprintf(log, "[%s]::%s::[%s]::%s\n", status, timestamp, tag, info);
       fclose(log);
   }
}
```
Fungsi ini digunakan untuk menulis ke file log dengan mencatat status, timestamp, tag, dan informasi terkait suatu kegiatan dalam sistem.

-	Fungsi Move Files
```c
static int move_files(const char *source, const char *target, const char *ext, const char *category) {
   DIR *dir = opendir(source);
   if (dir == NULL) {
       perror("gagal membuka source directory");
       return -1;
   }

   struct dirent *entry;
   char source_path[PATH_MAX];
   char target_path[PATH_MAX];

   while ((entry = readdir(dir)) != NULL) {
       if (entry->d_type == DT_REG) { 
           char *file_ext = strrchr(entry->d_name, '.');
           if (file_ext != NULL && strcmp(file_ext, ext) == 0) {
               snprintf(target_path, PATH_MAX, "%s/%s", target, category);
               mkdir(target_path, 0755);

               snprintf(source_path, PATH_MAX, "%s/%s", source, entry->d_name);
               snprintf(target_path, PATH_MAX, "%s/%s/%s", target, category, entry->d_name);
               rename(source_path, target_path);

               write_log("SUCCESS", "move_files", entry->d_name);
           }
       }
   }

   closedir(dir);
   return 0;
}
```
Memindahkan file dari direktori sumber ke direktori target berdasarkan ekstensi file dan kategori. Selama proses pemindahan, fungsi ini juga mencatat ke dalam file log.

-	Fungsi Main
```c
int main(int argc, char *argv[]) {
   if (argc != 2) {
       fprintf(stderr, "Usage: %s <mountpoint>\n", argv[0]);
       return EXIT_FAILURE;
   }

   if (move_files(source_dir, target_dir, ".pdf", "documents") != 0 ||
       move_files(source_dir, target_dir, ".docx", "documents") != 0 ||
       move_files(source_dir, target_dir, ".jpg", "images") != 0 ||
       move_files(source_dir, target_dir, ".png", "images") != 0 ||
       move_files(source_dir, target_dir, ".ico", "images") != 0 ||
       move_files(source_dir, target_dir, ".js", "website") != 0 ||
       move_files(source_dir, target_dir, ".html", "website") != 0 ||
       move_files(source_dir, target_dir, ".json", "website") != 0 ||
       move_files(source_dir, target_dir, ".c", "sisop") != 0 ||
       move_files(source_dir, target_dir, ".sh", "sisop") != 0 ||
       move_files(source_dir, target_dir, ".txt", "text") != 0 ||
       move_files(source_dir, target_dir, ".ipynb", "aI") != 0 ||
       move_files(source_dir, target_dir, ".csv", "aI") != 0) {
       fprintf(stderr, "Error moving files\n");
       return EXIT_FAILURE;
   }

   return fuse_main(argc, argv, &custom_oper, NULL);
}
```
Melakukan pemindahan file dengan memanggil move_files untuk setiap ekstensi dan kategori yang diinginkan, dan menjalankan fuse dengan operasi yang telah diimplementasikan.


### Kendala
---
1. Pemindahan file menggunakan fuse masih kurang tepat, dan langkah selanjutnya belum terselesaikan. 

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Memperbaiki yang masih belum tepat, dan menyelesaikannya.

### Hasil
---

1. Hasil Run Open-password.c `Open-password.c`

![hasil File 'Open-password.c'](img/Soal_2/1.png)

2. Hasil File `logs-fuse.log` dan Pemindahan File ke Directory 

![hasil File ](img/Soal_2/2.png)

## Soal 3

### Study case soal 3
---
Dalam kegelapan malam yang sepi, bulan purnama menerangi langit dengan cahaya peraknya. Rumah-rumah di sekitar daerah itu terlihat seperti bayangan yang menyelimuti jalan-jalan kecil yang sepi. Suasana ini terasa aneh, membuat hati seorang wanita bernama Sarah merasa tidak nyaman.

Dia berjalan ke jendela kamarnya, menatap keluar ke halaman belakang. Pohon-pohon besar di halaman tampak gelap dan menyeramkan dalam sinar bulan. Sesekali, cahaya bulan yang redup itu memperlihatkan bayangan-bayangan aneh yang bergerak di antara pepohonan.

Tiba-tiba, Ting! Pandangannya tertuju pada layar smartphonenya. Dia terkejut bukan kepalang. Notifikasi barusan adalah remainder pengumpulan praktikum yang dikirim oleh asisten praktikumnya. Sarah lupa bahwa 2 jam lagi adalah waktu pengumpulan tugas praktikum mata kuliah Sistem Operasi. Sarah sejenak merasa putus asa, saat dia menyadari bahwa ketidaknyamanan yang dirasakannya adalah akibat tekanan tugas praktikumnya.

Sarah melihat tugas praktikum yang harus dia selesaikan, dan dalam hitungan detik, rasa panik melanda. Tugas ini tampaknya sangat kompleks, dan dia belum sepenuhnya siap. Sebagai seorang mahasiswa teknik komputer, mata kuliah Sistem Operasi adalah salah satu mata kuliah yang sangat menuntut, dan dia harus menyelesaikan tugas ini dengan baik untuk menjaga nilai akademiknya.


### Problem
---
Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:

- Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 

- Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.

- Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
    - Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
    - Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
    - Format untuk logging yaitu sebagai berikut.
    **[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]**  
    
- **Contoh:**
REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
FLAG::231105-12:29:33::RMDIR::/home/sarah/folder

- Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
**Contoh:**
file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

Contoh:
- /tmp/test_fuse/ adalah filesystem yang harus dirancang.
- /home/index/modular/ adalah direktori asli.

### Solution
---
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <regex.h>



static const char *dirpath = "/home/dhani/modular";

void log_command(const char *level, const char *cmd, const char *desc, ...)
{
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/dhani/fs_module.log", "a");
    if (log_file != NULL)
    {
        va_list args;
        va_start(args, desc);

        fprintf(log_file, "[%s]::%s::%s::", level, timestamp, cmd);
        vfprintf(log_file, desc, args);
        fprintf(log_file, "\n");

        va_end(args);

        fclose(log_file);
    }
}



void modularize_file(const char *path)
{
    FILE *input_file = fopen(path, "rb");
    if (input_file == NULL)
    {
        return;
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    rewind(input_file);

    if (file_size <= 1024)
    {
        fclose(input_file);
        return;
    }

    int count = (file_size + 1023) / 1024;

    for (int i = 1; i <= count; i++)
    {
        char chunk_path[256];
        snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", path, i);
        FILE *output_file = fopen(chunk_path, "wb");

        if (output_file == NULL)
        {
            fclose(input_file);
            return;
        }

        char *buffer = (char *)malloc(1024);
        if (buffer == NULL)
        {
            fclose(input_file);
            fclose(output_file);
            return;
        }

        size_t bytes = fread(buffer, 1, 1024, input_file);
        fwrite(buffer, 1, bytes, output_file);

        free(buffer);
        fclose(output_file);
    }

    fclose(input_file);
}



static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
    {
        return -errno;
    }

    return 0;
}



int dot_3_digit(const char *filename)
{
    const char *pattern = ".*\\.[0-9]{3}$";
    regex_t regex;

    if (regcomp(&regex, pattern, REG_EXTENDED))
        return 0;

    int match = !regexec(&regex, filename, 0, NULL, 0);
    regfree(&regex);

    return match;
}

void deleteFiles(const char *path)
{
    DIR *dir = opendir(path);

    if (dir == NULL)
    {
        perror("Error opening directory");
        return;
    }

    struct dirent *entry;

    while ((entry = readdir(dir)) != NULL)
    {
        if (dot_3_digit(entry->d_name))
        {
            char full_path[1000];
            sprintf(full_path, "%s/%s", path, entry->d_name);

            if (remove(full_path) != 0)
            {
                perror("Error deleting file");
            }
            else
            {
                printf("Deleted file: %s\n", full_path);
            }
        }
    }

    closedir(dir);
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
    {
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char full_path[1256];
        sprintf(full_path, "%s/%s", fpath, de->d_name);

        if (strstr(fpath, "/module_"))
        {
            if (de->d_type == DT_REG && !dot_3_digit(de->d_name))
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                modularize_file(full_path);
            }
        }
        else
        {
            if (de->d_type == DT_DIR || de->d_type == DT_REG)
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                deleteFiles(fpath);
            }
        }
    }

    closedir(dp);

    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
    {
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
    {
        res = -errno;
    }

    close(fd);

    return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags, mode);

    if (res == -1)
    {
        log_command("REPORT", "CREATE", "Error creating file %s", fpath);
        return -errno;
    }

    fi->fh = res;

    log_command("REPORT", "CREATE", fpath);

    return 0;
}



static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = fi->fh;
    int res = pwrite(fd, buf, size, offset);

    if (res == -1)
    {
        log_command("REPORT", "WRITE", "Error writing to file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "WRITE", "Write to file %s", fpath);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_command("REPORT", "MKDIR", "Error creating directory %s", fpath);
        return -errno;
    }

    log_command("REPORT", "MKDIR", "Directory created: %s", fpath);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000];
    char to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        log_command("REPORT", "RENAME", "Error renaming %s to %s", from_path, to_path);
        return -errno;
    }

    log_command("REPORT", "RENAME", "%s to %s", from_path, to_path);

    return 0;
}



static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = rmdir(fpath);

    if (res == -1)
    {
        log_command("FLAG", "RMDIR", "Error removing directory %s", fpath);
        return -errno;
    }

    log_command("FLAG", "RMDIR", fpath);

    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = unlink(fpath);

    if (res == -1)
    {
        log_command("REPORT", "UNLINK", "Error unlinking file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "UNLINK", fpath);

    return 0;
}

static int xmp_utimens(const char *path, const struct timespec tv[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = utimensat(0, fpath, tv, AT_SYMLINK_NOFOLLOW);
    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .create = xmp_create,
    .write = xmp_write,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
};


int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```



### Kendala
---
1. Salah konsep belum menggunakan Fuse.

### Revisi
---
> Menambahkan fuse pada program


### Hasil
---

1. Hasil `easy.c`

![hasil File 'easy.c'](img/Soal_3/hasil.png)

