#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>


typedef struct {
    char level[10];
    char timestamp[20];
    char cmd[20];
    char desc[256];
} LogEntry;



void logEntry(char* level, char* cmd, char* oldName, char* newName) {
    LogEntry log;
    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(log.timestamp, sizeof(log.timestamp), "%y%m%d-%H:%M:%S", timeinfo);
    strcpy(log.level, level);
    strcpy(log.cmd, cmd);

    snprintf(log.desc, sizeof(log.desc), "%s::%s", oldName, newName);

    FILE* logFile = fopen("/home/dhanixyi/fs_module.log", "a");
    if (logFile == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    fprintf(logFile, "[%s]::%s::%s::%s\n", log.level, log.timestamp, log.cmd, log.desc);
    fclose(logFile);
}



void modularizeFile(char* filePath) {
    FILE* originalFile = fopen(filePath, "rb");
    if (originalFile == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    fseek(originalFile, 0, SEEK_END);
    long fileSize = ftell(originalFile);
    rewind(originalFile);

    int numChunks = (fileSize + 1023) / 1024;

    for (int i = 0; i < numChunks; i++) {
        char chunkFileName[256];
        sprintf(chunkFileName, "%s.%03d", filePath, i);

        FILE* chunkFile = fopen(chunkFileName, "wb");
        if (chunkFile == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
        }

        char buffer[1024];
        size_t bytesRead = fread(buffer, 1, sizeof(buffer), originalFile);
        fwrite(buffer, 1, bytesRead, chunkFile);

        fclose(chunkFile);
    }

    fclose(originalFile);
}



void demodularizeFile(char* filePath) {
    FILE* outputFile = fopen(filePath, "wb");
    if (outputFile == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    int i = 0;
    while (1) {
        char chunkFileName[256];
        sprintf(chunkFileName, "%s.%03d", filePath, i);

        FILE* chunkFile = fopen(chunkFileName, "rb");
        if (chunkFile == NULL) {
            break;
        }

        char buffer[1024];
        size_t bytesRead = fread(buffer, 1, sizeof(buffer), chunkFile);
        fwrite(buffer, 1, bytesRead, outputFile);

        fclose(chunkFile);
        i++;
    }

    fclose(outputFile);
}



void modularizeDirectory(char* directory) {
    DIR* dir;
    struct dirent* entry;

    dir = opendir(directory);
    if (dir == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strncmp(entry->d_name, "module_", 7) == 0) {
                char subdir[256];
                sprintf(subdir, "%s/%s", directory, entry->d_name);
                modularizeDirectory(subdir);
            }
        } else if (entry->d_type == DT_REG) {
            char filePath[256];
            sprintf(filePath, "%s/%s", directory, entry->d_name);
            modularizeFile(filePath);
        }
    }

    closedir(dir);
}



void demodularizeDirectory(char* directory) {
    DIR* dir;
    struct dirent* entry;

    dir = opendir(directory);
    if (dir == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strncmp(entry->d_name, "module_", 7) == 0) {
                char subdir[256];
                sprintf(subdir, "%s/%s", directory, entry->d_name);
                demodularizeDirectory(subdir);
            }
        } else if (entry->d_type == DT_REG) {
            char filePath[256];
            sprintf(filePath, "%s/%s", directory, entry->d_name);
            demodularizeFile(filePath);
        }
    }

    closedir(dir);
}





int main() {

    modularizeDirectory("/tmp/test_fuse/");

    char oldName[] = "/home/dhanixyi/cobates.jpg";
    char newName[] = "/home/dhanixyi/okecoba.jpg";
    if (rename(oldName, newName) == 0) {
        logEntry("REPORT", "RENAME", oldName, newName);
    } else {
        perror("rename");
    }



    char fileName[] = "/home/dhanixyi/test.txt";
    FILE* file = fopen(fileName, "w");
    if (file != NULL) {
        fclose(file);
        logEntry("REPORT", "CREATE", fileName, "");
    } else {
        perror("fopen");
    }



    char dirName[] = "/home/dhanixyi/folder";
    if (rmdir(dirName) == 0) {
        logEntry("FLAG", "RMDIR", dirName, "");
    } else {
        perror("rmdir");
    }



    char fileToDelete[] = "/home/dhanixyi/test.txt";
    if (unlink(fileToDelete) == 0) {
        logEntry("FLAG", "UNLINK", fileToDelete, "");
    } else {
        perror("unlink");
    }



    demodularizeDirectory("/tmp/test_fuse/");

    return 0;
}
